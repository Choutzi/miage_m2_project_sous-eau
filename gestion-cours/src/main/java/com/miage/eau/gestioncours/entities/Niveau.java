package com.miage.eau.gestioncours.entities;

public enum Niveau {
    NIVEAU_1, NIVEAU_2, NIVEAU_3, NIVEAU_4, NIVEAU_5;
}
