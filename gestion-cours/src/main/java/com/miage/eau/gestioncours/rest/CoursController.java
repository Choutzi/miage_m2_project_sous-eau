package com.miage.eau.gestioncours.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miage.eau.gestioncours.entities.Cours;
import com.miage.eau.gestioncours.repositories.CoursRepository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/cours")
public class CoursController {
    private static final Logger logger = LoggerFactory.getLogger(CoursController.class);

    @Autowired
    private CoursRepository repo;

    @GetMapping("{idCours}")
    public Cours getCours(@PathVariable("idCours") String idCours) {
        Optional<Cours> cours = repo.findById(idCours);
        if(cours.isPresent()){
            logger.info("cours récupéré");
            return cours.get();
        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Cours inconnu");
        }
    }

    @PostMapping("/create")
    public Cours postCours(@RequestBody Cours cours) {
	logger.info("Create cours : " + cours);
	return repo.save(cours);
    }
    
    @PutMapping("inscription/{id}-{login}")
    void inscriptionCours(@PathVariable("id") String idCours, @PathVariable("login") String login) {
        Optional<Cours> cours = repo.findById(idCours);
        if (cours.isPresent()) {
            Cours c = cours.get();
            c.getParticipants().add(login);
            repo.save(c);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Cours inconnu");
        }
    }
    @GetMapping("/coursPositione")
    int getNBCoursPositione(){
        int nbCoursPositione =0;
        for(Cours c: repo.findAll()){   
            if(c.getDate()!=null){
                nbCoursPositione++;
            }
        }
        return nbCoursPositione;
    }

}
