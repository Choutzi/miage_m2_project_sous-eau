package com.miage.eau.gestioncours.entities;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Document(collection = "cours")
public class Cours {

    /*
     * Id du cours
     */
    @Id
    public String idCours;

    /*
     * Le nom du cours
     */
    @Field
    private String nom;

    /*
     * Le niveau du cours
     */
    @Field
    private Niveau niveau;

    /*
     * La date du crénau du cours
     */
    @Field
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private LocalDate date;

    /*
     * id de l'enseignant
     */
    @Field
    private String idEnseignant;

    /*
     * id du lieu
     */
    @Field
    private String idLieu;

    /*
     * Duree du cours
     */
    @Field
    private int dureeMinute;

    /*
     * Liste des participants au cours
     */
    @Field
    private List<String> participants;
}
