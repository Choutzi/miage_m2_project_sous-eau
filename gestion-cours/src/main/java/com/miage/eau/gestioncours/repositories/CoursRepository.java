package com.miage.eau.gestioncours.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.miage.eau.gestioncours.entities.Cours;
import java.time.LocalDate;
import java.util.Collection;
import org.springframework.data.repository.query.Param;

@Repository
public interface CoursRepository extends MongoRepository<Cours, String> {
}
