# Miage_M2_Project_Sous-Eau

Plateforme web complète pour la gestion d’un club de plongée. La plateforme « les MIAGistes sous l’eau » permet à l’association de plongée de gérer ses membres, de planifier des cours et de gérer le prêt de matériel. 

Pour lancer le projet il suffit de se mettre dans le répertoire racine et de lancer **"docker-compose up -d"** afin de lancer les différents containers microservice

Si vous modifiez le projet il faudrait re-build les images avec **"docker-compose build"** avant de relancer **"docker-compose up -d"**

Vous trouverez l'api Rest à cette adresse : 
[API REST](https://documenter.getpostman.com/view/9837071/T17AiAAs)