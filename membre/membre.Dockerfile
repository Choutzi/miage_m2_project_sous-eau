# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
FROM maven:3.6-jdk-8 AS build  
COPY src /usr/src/app/src  
COPY pom.xml /usr/src/app  
RUN mvn -f /usr/src/app/pom.xml clean package

FROM openjdk:8 
COPY --from=build /usr/src/app/target/membre.jar /usr/app/membre.jar   
ENTRYPOINT ["java","-jar","/usr/app/membre.jar"]  