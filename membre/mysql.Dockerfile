# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
FROM mysql:8

ENV MYSQL_DATABASE=membres
ENV MYSQL_ROOT_PASSWORD=pwd

COPY mysqlCustom.cnf /etc/mysql/conf.d/custom.cnf