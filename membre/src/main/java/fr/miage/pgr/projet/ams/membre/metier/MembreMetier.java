/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.pgr.projet.ams.membre.metier;

import fr.miage.pgr.projet.ams.membre.entities.Maitrise;
import fr.miage.pgr.projet.ams.membre.repositories.MembreRepo;
import fr.miage.pgr.projet.ams.membre.entities.Membre;
import fr.miage.pgr.projet.ams.membre.entities.Role;
import java.util.Collection;

/**
 *
 * @author remil
 */
public interface MembreMetier {
    
    public Membre creerMembre(Membre m);
    public Membre getMembre(String login);
    public void ajouterIban(String login, String iban);
    public void validerPaiement(String login);
    public void ajouterMedicalCertif(String login);
    public void ajouterLicence(String login, String licenceNum);
    public void modfierMaitrise(String login, Maitrise maitrise);
    public void changerRole(String login, Role role);
    public Collection<Membre> getAllMembers();
    public Collection<Membre> getAllEnseignants();
    public int getCotisationsPrevues();
    public int getCotisationsReglees();
    
}
