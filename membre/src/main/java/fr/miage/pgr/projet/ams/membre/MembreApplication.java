package fr.miage.pgr.projet.ams.membre;

import fr.miage.pgr.projet.ams.membre.entities.Maitrise;
import fr.miage.pgr.projet.ams.membre.entities.Membre;
import fr.miage.pgr.projet.ams.membre.entities.Role;
import fr.miage.pgr.projet.ams.membre.repositories.MembreRepo;
import java.time.LocalDate;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MembreApplication {

    @Autowired
    private MembreRepo membreRepo;

	public static void main(String[] args) {
		SpringApplication.run(MembreApplication.class, args);
	}

    @PostConstruct
    public void init() {
        membreRepo.save(new Membre("log", "Laroche", "Rémi", "r@gmail.com","iban", "mdp", "Toulouse","France","12234556",LocalDate.now(),true,Maitrise.NIVEAU_1,Role.Enseignant));
      }

}
