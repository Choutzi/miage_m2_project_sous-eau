/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.pgr.projet.ams.membre.entities;

/**
 *
 * @author remil
 */
public enum Maitrise {
    NIVEAU_1,
    NIVEAU_2,
    NIVEAU_3,
    NIVEAU_4,
    NIVEAU_5
}
