/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.pgr.projet.ams.membre.rest;

import fr.miage.pgr.projet.ams.membre.entities.Maitrise;
import fr.miage.pgr.projet.ams.membre.repositories.MembreRepo;
import fr.miage.pgr.projet.ams.membre.entities.Membre;
import fr.miage.pgr.projet.ams.membre.entities.Role;
import fr.miage.pgr.projet.ams.membre.metier.MembreMetier;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author remil
 */

@RestController
@RequestMapping("/api/gestion-membre")
public class RestMembre {
    
    @Autowired
    MembreRepo membreRepository;
    
    @Autowired
    MembreMetier membreMetier;
    
    @GetMapping("/membres")
    public Iterable<Membre> getListMembres(){
        return this.membreMetier.getAllMembers();
    }
    
    @GetMapping("/enseignants")
    public Iterable<Membre> getListEnseignants(){
        return this.membreMetier.getAllEnseignants();
    }
    
    @GetMapping("/membres/{login}")
    public Membre getUnMembre(@PathVariable("login") String login){
        return this.membreMetier.getMembre(login);
    }
    
    @GetMapping("/cotisationReglees")
    public int getCotisationsReglees(){
        return this.membreMetier.getCotisationsReglees();
    }
    
    @GetMapping("/cotisationPrevues")
    public int getCotisationsPrevues(){
        return this.membreMetier.getCotisationsPrevues();
    }
    
    @PostMapping("/create")
    public Membre createMember(@RequestBody Membre membre){
        return this.membreMetier.creerMembre(membre);
    }
    
    @PutMapping("/changerole/{login}-{role}")
    public void changerRole(@PathVariable("login") String login, @PathVariable("role") String role){
        this.membreMetier.changerRole(login, Role.valueOf(role));
    }
    
    @PutMapping("/modifierMaitrise/{login}-{maitrise}")
    public void modifierMaitrise(@PathVariable("login") String login, @PathVariable("maitrise") String maitrise){
        this.membreMetier.modfierMaitrise(login, Maitrise.valueOf(maitrise));
    }
    
    @PutMapping("/ajouterLicence/{login}-{licenceNumber}")
    public void ajouterLicence(@PathVariable("login") String login, @PathVariable("licenceNumber") String licenceNumber){
        this.membreMetier.ajouterLicence(login, licenceNumber);
    }
    
    @PutMapping("/ajouterMedicalCertif/{login}")
    public void ajouterMedicalCertif(@PathVariable("login") String login){
        this.membreMetier.ajouterMedicalCertif(login);
    }
    
    @PutMapping("/ajouterIban/{login}-{iban}")
    public void ajouterIban(@PathVariable("login") String login, @PathVariable("iban") String iban){
        this.membreMetier.ajouterIban(login, iban);
    }
    
    @PutMapping("/validerPaiement/{login}")
    public void validerPaiement(@PathVariable("login") String login){
        this.membreMetier.validerPaiement(login);
    }
    // test
    
}
