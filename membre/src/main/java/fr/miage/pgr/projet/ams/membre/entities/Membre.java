/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.pgr.projet.ams.membre.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.sun.istack.NotNull;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 *
 * @author remil
 */

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Membre implements Serializable {
    
   @Id
   @Column(unique = true)
   public String login;
   
   @NotNull
   public String nom;
   
   @NotNull
   public String prenom;
   
   @NotNull
   @Column(unique = true)
   public String mel;
   
   @NotNull
   public String iban;
   
   @NotNull
   public String mdp;
   
   @NotNull
   public String ville;
   
   @NotNull
   public String pays;  
   
   @Column(unique = true)
   public String licenceNum;
      
   @NotNull
   @JsonFormat(pattern = "yyyy-MM-dd")
   @JsonDeserialize(using = LocalDateDeserializer.class)
   @JsonSerialize(using = LocalDateSerializer.class)
   public LocalDate certifMedical;
   
   @NotNull
   public boolean paiement;
   
   @Enumerated(EnumType.STRING)
   @NotNull
   public Maitrise maitrise;
   
   @Enumerated(EnumType.STRING)
   @NotNull
   public Role role;
    
}
