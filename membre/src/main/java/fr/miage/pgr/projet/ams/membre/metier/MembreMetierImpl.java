/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.pgr.projet.ams.membre.metier;


import fr.miage.pgr.projet.ams.membre.entities.Maitrise;
import fr.miage.pgr.projet.ams.membre.entities.Membre;
import fr.miage.pgr.projet.ams.membre.entities.Role;
import fr.miage.pgr.projet.ams.membre.repositories.MembreRepo;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


/**
 *
 * @author remil
 */

@Service
public class MembreMetierImpl implements MembreMetier{
    @Autowired
    MembreRepo membreRepository;
    
    @Autowired
    MembreMetier membreMetier;
    
    // permet de créer un membre 
    // Entrée : Membre
    // Sortie : Membre
    @Override
    public Membre creerMembre(Membre m){
        membreRepository.save(m);
        return m;
    }
    
    // permet de récupérer un membre 
    // Entrée : Login
    // Sortie : Membre
    @Override
    public Membre getMembre(String login){
        Optional<Membre> membre = membreRepository.findById(login);
        if(membre.isPresent()){
            return membre.get();
        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Membre inconnu");
        }
    }
    
    // permet d'ajouter un Iban au membre
    // Entrée : Login et iban
    @Override
    public void ajouterIban(String login, String iban) {
        Optional<Membre> membre = membreRepository.findById(login);
        if (membre.isPresent()) {
            Membre m = membre.get();
            m.setIban(iban);
            membreRepository.save(m);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Membre inconnu");
        }
    }
    
    
    // permet de valider le Paiement
    // Entrée : Login
    @Override
    public void validerPaiement(String login) {
        Optional<Membre> membre = membreRepository.findById(login);
        if (membre.isPresent()) {
            Membre m = membre.get();
            m.setPaiement(true);
            membreRepository.save(m);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Membre inconnu");
        }
    }
    
    // permet de récupérer un membre 
    // Entrée : Login
    @Override
    public void ajouterMedicalCertif(String login) {
        Optional<Membre> membre = membreRepository.findById(login);
        if (membre.isPresent()) {
            Membre m = membre.get();
            m.setCertifMedical(LocalDate.now());
            membreRepository.save(m);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Membre inconnu");
        }
    }
    
    // permet de ajouter un numéro de licence au Membre
    // Entrée : Login
    @Override
    public void ajouterLicence(String login, String licenceNum) {
        Optional<Membre> membre = membreRepository.findById(login);
        if (membre.isPresent()) {
            Membre m = membre.get();
            m.setLicenceNum(licenceNum);
            membreRepository.save(m);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Membre inconnu");
        }
    }
    
    // permet de modifier le niveau de maitrise du Membre
    // Entrée : Login et maitrise
    @Override
    public void modfierMaitrise(String login, Maitrise maitrise) {
        Optional<Membre> membre = membreRepository.findById(login);
        if (membre.isPresent()) {
            Membre m = membre.get();
            m.setMaitrise(maitrise);
            membreRepository.save(m);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Membre inconnu");
        }
    }

    // permet de modifier le role du membre
    // Entrée : Login et role
    @Override
    public void changerRole(String login, Role role) {
        Optional<Membre> membre = membreRepository.findById(login);
        if (membre.isPresent()) {
            Membre m = membre.get();
            m.setRole(role);
            membreRepository.save(m);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Membre inconnu");
        }
    }
    
    // permet de récupérer tout les membres
    // Sortie : Membres
    @Override
    public Collection<Membre> getAllMembers() {
        Collection<Membre> members = new ArrayList<>();
        membreRepository.findAll().forEach(members::add);
        return members;
    }
     
    // permet de récupérer tout les enseignants
    // Sortie : Membres ( enseignants )
    @Override
    public Collection<Membre> getAllEnseignants() {
        Collection<Membre> enseignants = membreRepository.findByRole(Role.Enseignant);
        return enseignants;
    }
    
    // permet de récupérer le nombre de cotisations prévues
    // Sortie : int 
    @Override
    public int getCotisationsPrevues() {
        Collection<Membre> membresCotisationsPrevues = membreRepository.findByPaiement(false);
        int nbCotisationsPrevues = membresCotisationsPrevues.size();
        return nbCotisationsPrevues;
    }
    
    // permet de récupérer le nombre de cotisations réglées
    // Sortie : int 
    @Override
    public int getCotisationsReglees() {
        Collection<Membre> membresCotisationsPrevues = membreRepository.findByPaiement(true);
        int nbCotisationsPrevues = membresCotisationsPrevues.size();
        return nbCotisationsPrevues;
    }
}
