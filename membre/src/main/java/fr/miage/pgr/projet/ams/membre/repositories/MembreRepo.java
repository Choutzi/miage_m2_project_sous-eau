/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.pgr.projet.ams.membre.repositories;

import fr.miage.pgr.projet.ams.membre.entities.Maitrise;
import fr.miage.pgr.projet.ams.membre.entities.Membre;
import fr.miage.pgr.projet.ams.membre.entities.Role;
import java.util.Collection;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author remil
 */

@Repository
public interface MembreRepo extends CrudRepository<Membre, String>{
    Collection<Membre> findByRole(@Param("role") Role role);
    Collection<Membre> findByPaiement(@Param("paiement") Boolean paiement);
}
