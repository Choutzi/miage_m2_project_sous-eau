package com.miage.eau.webapp.transientobj;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class PiscineDeserializer extends StdDeserializer<Piscine> {

    public PiscineDeserializer(Class<?> vc) {
	super(vc);
    }

    @Override
    public Piscine deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
	Logger lg = LoggerFactory.getLogger(getClass());
	JsonNode nodet = (JsonNode) p.getCodec().readTree(p).get("records");

	/*
	 * Prblème au niveau des node qui affiche bien le json sur le toString mais qui
	 * ne donne rien avec des get
	 */
	String[] st = nodet.toString().split(",");
	String id = "";
	for (String s : st) {
	    if (s.contains("nom_complet")) {
		id = s.split("\"")[3];
	    }
	}
	lg.info(id);
	return new Piscine(id);
    }

}
