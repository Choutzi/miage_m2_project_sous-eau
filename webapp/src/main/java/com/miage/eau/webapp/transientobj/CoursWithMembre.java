package com.miage.eau.webapp.transientobj;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class CoursWithMembre extends Cours {
    List<Membre> membres = new ArrayList<>();

    public void addMembre(Membre m) {
	if (this.checkDate() && membres.size() < PARTICIPANTS_MAX && this.niveau.compareTo(m.getMaitrise()) == 0
		&& !this.membres.contains(m)) {
	    this.membres.add(m);
	    if (!this.participants.contains(m.getLogin())) {
		this.participants.add(m.getLogin());
	    }
	}
    }
}
