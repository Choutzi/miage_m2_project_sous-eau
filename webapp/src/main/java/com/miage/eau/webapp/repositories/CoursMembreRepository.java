package com.miage.eau.webapp.repositories;


import com.miage.eau.webapp.transientobj.Cours;
import com.miage.eau.webapp.transientobj.CoursWithMembre;
import com.miage.eau.webapp.transientobj.Maitrise;
import com.miage.eau.webapp.transientobj.Membre;
import java.util.Collection;

import com.miage.eau.webapp.transientobj.Membre;
import com.miage.eau.webapp.transientobj.Piscine;

public interface CoursMembreRepository {

    Piscine getPiscine(String id);

    Collection<Membre> getAllMembre();

    Collection<Membre> getAllEnseignant();

    Membre getUnMembre(String login);

    Membre createMembre(Membre m);

    void ajouterMedicalCertif(String login);

    void ajouterLicence(String login, String licenceNumber);

    void modifierMaitrise(String login, String maitrise, String role);

    void ajouterIban(String login, String iban);

    void validerPaiement(String login, String role);

    void changerRole(String login, String roleModif, String role);

    int getCotisationReglees();

    int getCotisationPrevuees();
    void creerCours(Cours cours);
    void inscriptionCours(String idCours, String login);
    int getCoursPositione();
}
