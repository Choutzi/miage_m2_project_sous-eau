package com.miage.eau.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.miage.eau.webapp.repositories.CoursMembreRepository;
import com.miage.eau.webapp.repositories.CoursMembreRepositoryImpl;
import com.miage.eau.webapp.transientobj.Piscine;
import com.miage.eau.webapp.transientobj.PiscineDeserializer;

@SpringBootApplication
public class WebappApplication {

    public static final String COURS_SERVICE_URL = "http://gestion-cours:8080/cours/";

    public static final String MEMBRE_SERVICE_URL = "http://gestion-membres:8080/api/gestion-membre/";

    public static final String PISCINES_API_URL = "https://data.toulouse-metropole.fr/api/records/1.0/search/?dataset=piscines&q=";

    public static void main(String[] args) {
	SpringApplication.run(WebappApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
	/* Ajout du deserializer pour les piscine répurérée dans l'api */
	ObjectMapper mapper = new ObjectMapper();
	SimpleModule module = new SimpleModule("PiscineDeserializer", new Version(1, 0, 0, null, null, null));
	module.addDeserializer(Piscine.class, new PiscineDeserializer(null));
	mapper.registerModule(module);
	MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
	converter.setObjectMapper(mapper);
	RestTemplate rest = new RestTemplate();
	rest.getMessageConverters().add(0, converter);
	return rest;
    }

    @Bean
    public CoursMembreRepository coursRepository() {
	return new CoursMembreRepositoryImpl(COURS_SERVICE_URL, MEMBRE_SERVICE_URL, PISCINES_API_URL);
    }

}
