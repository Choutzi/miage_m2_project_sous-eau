package com.miage.eau.webapp.transientobj;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import java.io.Serializable;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Membre implements Serializable {

   public String login;
   
   public String nom;
   
   public String prenom;
   
   public String mel;
   
   public String iban;
   
   public String mdp;
   
   public String ville;
   
   public String pays;  
   
   public String licenceNum;
      
   @JsonFormat(pattern = "yyyy-MM-dd")
   @JsonDeserialize(using = LocalDateDeserializer.class)
   @JsonSerialize(using = LocalDateSerializer.class)
   public LocalDate certifMedical;

   public Boolean paiement;
   
   public Maitrise maitrise;
   
   public Role role;

}
