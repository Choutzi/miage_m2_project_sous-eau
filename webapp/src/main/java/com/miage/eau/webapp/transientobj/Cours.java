package com.miage.eau.webapp.transientobj;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Cours implements Serializable {

    protected static final Integer PARTICIPANTS_MAX = 2;

    public String id;

    public String nom;

    public String idEnseignant;

    public LocalDate date;

    public List<String> participants;

    public Maitrise niveau;

    public String idPiscine;

    public boolean checkDate() {
	if (LocalDate.now().compareTo(this.date) > 0) {
	    return false;
	}
	return true;
    }
}
