package com.miage.eau.webapp.repositories;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.miage.eau.webapp.transientobj.Cours;
import com.miage.eau.webapp.transientobj.CoursWithMembre;
import com.miage.eau.webapp.transientobj.Maitrise;
import com.miage.eau.webapp.transientobj.Membre;
import com.miage.eau.webapp.transientobj.Piscine;
import com.miage.eau.webapp.transientobj.Role;
import java.util.Arrays;
import java.util.Collection;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class CoursMembreRepositoryImpl implements CoursMembreRepository {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    protected RestTemplate restTemplateCours;

    @Autowired
    protected RestTemplate restTemplateMembre;
    
    @Autowired
    protected RestTemplate restTemplatePiscine;

    protected String serviceUrlCours;
    protected String serviceUrlMembre;
    protected String apiUrlPiscine;


    public CoursMembreRepositoryImpl(String coursServiceUrl, String membreServiceUrl, String piscinesApiUrl) {
	this.serviceUrlCours = coursServiceUrl;
	this.serviceUrlMembre = membreServiceUrl;
	this.apiUrlPiscine = piscinesApiUrl;
    }


    @Override
    public Piscine getPiscine(String id) {
	logger.info("Piscine : " + id + " récupérée.");
	return restTemplatePiscine.getForObject(apiUrlPiscine + id, Piscine.class);
    }


    @Override
    public Collection<Membre> getAllMembre() {
        Membre[] membres = restTemplateMembre.getForObject(this.serviceUrlMembre+ "membres", Membre[].class);
        return Arrays.asList(membres);
    }
    
     @Override
    public Collection<Membre> getAllEnseignant() {
        Membre[] enseignant = restTemplateMembre.getForObject(this.serviceUrlMembre+ "enseignants", Membre[].class);
        return Arrays.asList(enseignant);
    }
    
    @Override
    public Membre getUnMembre(String login) {
        Membre membre = restTemplateMembre.getForObject(this.serviceUrlMembre+ "membres/"+login, Membre.class);
        return membre;
    }
    
    @Override
    public Membre createMembre(Membre m) {
        Membre membre = restTemplateMembre.postForObject(this.serviceUrlMembre+ "create",m, Membre.class);
        return membre;
    }

    // On simule ici le fait que le membre connecté soit une Secretaire ( dans un cas normal on devrait récuperer
    // les données de la personne connecté et non transmettre son role dans l'URL en parametre).
    @Override
    public void validerPaiement(String login, String role) {
        if(Role.valueOf(role)== Role.Secretaire){
        restTemplateCours.put(this.serviceUrlMembre+ "/validerPaiement/"+login, Membre.class);        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Seul les secretaires peuvent acceder à cette fonction");
        }
    }
    
    // On simule ici le fait que le membre connecté soit une Secretaire ( dans un cas normal on devrait récuperer
    // les données de la personne connecté et non transmettre son role dans l'URL en parametre).
    @Override
    public void modifierMaitrise(String login, String maitrise, String role) {
        if(Role.valueOf(role)== Role.Secretaire){
            restTemplateCours.put(this.serviceUrlMembre+ "/modifierMaitrise/"+login+"-"+maitrise.toString(), Membre.class);
        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Seul les secretaires peuvent acceder à cette fonction");
        }
    }    
    
    // On simule ici le fait que le membre connecté soit une Secretaire ( dans un cas normal on devrait récuperer
    // les données de la personne connecté et non transmettre son role dans l'URL en parametre).
    @Override
    public void changerRole(String login, String roleModif, String role) {
        if(Role.valueOf(role)== Role.Secretaire){
            restTemplateCours.put(this.serviceUrlMembre+ "/changerole/"+login+"-"+roleModif, Membre.class);
        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Seul les secretaires peuvent acceder à cette fonction");
        }
    }
    
    @Override
    public void ajouterLicence(String login, String licenceNumber) {
        restTemplateCours.put(this.serviceUrlMembre+ "/ajouterLicence/"+login+"-"+licenceNumber, Membre.class);
    }
    
    @Override
    public void ajouterIban(String login, String iban) {
        restTemplateCours.put(this.serviceUrlMembre+ "/ajouterIban/"+login+"-"+iban, Membre.class);
    }    
        
    @Override
    public void ajouterMedicalCertif(String login) {
            restTemplateCours.put(this.serviceUrlMembre+ "ajouterMedicalCertif/"+login, Membre.class);
    }
    
    @Override
    public int getCotisationPrevuees() {
        int nbCotisation = restTemplateMembre.getForObject(this.serviceUrlMembre+ "cotisationPrevues", Integer.class);
        return nbCotisation;
    }
    
    @Override
    public int getCotisationReglees() {
        int nbCotisation = restTemplateMembre.getForObject(this.serviceUrlMembre+ "cotisationReglees", Integer.class);
        return nbCotisation;
    }
    
    @Override
    public void creerCours(Cours cours) {
       if(getPiscine(cours.getIdPiscine()).idPiscine!=""){
        restTemplateCours.postForObject(this.serviceUrlCours+ "create",cours, Cours.class);
       }
    }
    
    @Override
    public void inscriptionCours(String idCours, String login) {
        restTemplateCours.put(this.serviceUrlCours+ "inscription/"+idCours+"-"+login, Cours.class);
    } 
    
    @Override
    public int getCoursPositione() {
        return restTemplateCours.getForObject(this.serviceUrlCours+ "coursPositione", Integer.class);
    }   
}