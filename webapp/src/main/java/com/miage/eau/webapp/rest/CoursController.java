package com.miage.eau.webapp.rest;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miage.eau.webapp.repositories.CoursMembreRepository;
import com.miage.eau.webapp.transientobj.Cours;
import com.miage.eau.webapp.transientobj.Membre;
import java.util.Collection;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.miage.eau.webapp.transientobj.Piscine;

/**
 * Service d'exposition des cours-membres
 * 
 * @author Choutzi
 *
 */
@RestController
@RequestMapping("/webapp")
public class CoursController {

    @Autowired
    CoursMembreRepository coursMembreRepository;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////// SECRETAIRE ONLY/////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    @GetMapping("/secretaire/membres")
    public Collection<Membre> getAllMembre() {
	return coursMembreRepository.getAllMembre();
    }

    @GetMapping("/secretaire/membres/{login}")
    public Membre getUnMembre(@PathVariable("login") String login) {
	return coursMembreRepository.getUnMembre(login);
    }

    @PostMapping("/secretaire/membres/create")
    public Membre createMembre(@RequestBody Membre M) {
	return coursMembreRepository.createMembre(M);
    }

    @PutMapping("membre/modifierMaitrise/{login}-{maitrise}-{role}")
    public void modifierMaitrise(@PathVariable("login") String login, @PathVariable("role") String role,
	    @PathVariable("maitrise") String maitrise) {
	this.coursMembreRepository.modifierMaitrise(login, maitrise, role);
    }

    @PutMapping("secretaire/validerPaiement/{login}-{role}")
    public void validerPaiement(@PathVariable("login") String login, @PathVariable("role") String role) {
	this.coursMembreRepository.validerPaiement(login, role);
    }

    @PutMapping("secretaire/changerrole/{login}-{roleModif}-{role}")
    public void changerRole(@PathVariable("login") String login, @PathVariable("roleModif") String roleModif,
	    @PathVariable("role") String role) {
	this.coursMembreRepository.changerRole(login, roleModif, role);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////// MEMBRE
    /////////////////////////////////////////////////////////////////////////////////////////////// ONLY//////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    @PutMapping("membre/ajouterMedicalCertif/{login}")
    public void ajouterMedicalCertif(@PathVariable("login") String login) {
	this.coursMembreRepository.ajouterMedicalCertif(login);
    }

    @PutMapping("membre/ajouterIban/{login}-{iban}")
    public void ajouterIban(@PathVariable("login") String login, @PathVariable("iban") String iban) {
	this.coursMembreRepository.ajouterIban(login, iban);
    }

    @PutMapping("membre/ajouterLicence/{login}-{licenceNumber}")
    public void ajouterLicence(@PathVariable("login") String login,
	    @PathVariable("licenceNumber") String licenceNumber) {
	this.coursMembreRepository.ajouterLicence(login, licenceNumber);
    }
    
    @PutMapping("membre/inscriptionCours/{login}-{idCours}")
    public void inscriptionCours(@PathVariable("login") String login,@PathVariable("idCours") String idCours){
        this.coursMembreRepository.inscriptionCours(idCours, login);
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////// PRESIDENT ONLY//////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @GetMapping("/president/membres")
    public int getAllMembre(String login) {
	return coursMembreRepository.getAllMembre().size();
    }

    @GetMapping("/president/enseignants")
    public int getAllEnseignant(String login) {
	return coursMembreRepository.getAllEnseignant().size();
    }

    @GetMapping("/president/cotisationPrevuees")
    public int getNBCotisationPrevuees() {
	return coursMembreRepository.getCotisationPrevuees();
    }

    @GetMapping("/president/cotisationReglees")
    public int getNBCotisationReglees() {
	return coursMembreRepository.getCotisationReglees();
    }
    
    @GetMapping("/president/coursPositione")
    public int getNBCoursPositione() {
	return coursMembreRepository.getCoursPositione();
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////ENSEIGNANTS ONLY/////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    
    @PostMapping("/enseignants/cours/create")
    public void creerCours(@RequestBody Cours cours) {
	coursMembreRepository.creerCours(cours);
    }
    
    // PISCINE
    @GetMapping("/piscines/{id}")
    public Piscine getPiscines(@PathVariable("id") String id) {
	return coursMembreRepository.getPiscine(id);
    }
}
